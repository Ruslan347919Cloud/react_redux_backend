<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\Article;
use app\models\Category;
use app\models\Tag;
use app\models\User;
use app\models\Comment;
use app\models\SiteSearch;
use yii\data\Pagination;

class SiteController extends Controller {

  public function behaviors() {
      return [
          'access' => [
              'class' => AccessControl::className(),
              'only' => ['logout'],
              'rules' => [
                  [
                      'actions' => ['logout'],
                      'allow' => true,
                      'roles' => ['@'],
                  ],
              ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'logout' => ['post'],
              ],
          ],
      ];
  }
  /**
   * {@inheritdoc}
   */
  public function actions()
  {
      return [
          'error' => [
              'class' => 'yii\web\ErrorAction',
          ],
          'captcha' => [
              'class' => 'yii\captcha\CaptchaAction',
              'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
          ],
      ];
  }
  /**
   * Displays contact page.
   *
   * @return Response|string
   */
  public function actionContact()
  {
      $model = new ContactForm();
      if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
          Yii::$app->session->setFlash('contactFormSubmitted');

          return $this->refresh();
      }
      return $this->render('contact', [
          'model' => $model,
      ]);
  }

	public function actionAbout() {
    return $this->render('about');
  }

	public function actionIndex() {
    $itemsOnPage = 3;
    $data = Article::selectAll($itemsOnPage);
    $articles = $data['articles'];
    $pagination = $data['pagination'];
    $latest = Article::getLatest();
    $categories = Category::getAll();
    $tags = Tag::getAll();
    $authors = User::getAll();
    
		return $this->render('index', compact(
      'articles', 'pagination', 'latest',
      'categories', 'tags', 'authors'));
	}

  public function actionViewSingle($id) {
    $commentModel = new Comment;
    $itemsOnPage = 4;
    $article = Article::selectOne($id);
    $article->updateCounters(['viewed' => 1]);
    $latest = Article::getLatest();
    $categories = Category::getAll();
    $tags = Tag::getAll();
    $authors = User::getAll();
    $data = Comment::getAllForArticle($id, $itemsOnPage);
    $comments = $data['comments'];
    $pagination = $data['pagination'];

  	return $this->render('single', compact(
      'article', 'comments', 'pagination', 'latest',
      'categories', 'tags', 'authors', 'commentModel'));
  }

  public function actionComment($id) {
      $commentModel = new Comment($id);
      if ($commentModel->load(Yii::$app->request->post())
          && $commentModel->save()) {
          Yii::$app->session->setFlash('comment', 'Your comment was sent to server. It will appear here soon.');
          $this->redirect(['site/view-single', 'id' => $id]);
      }
  }

  public function actionCategory($id) {
    $itemsOnPage = 3;
    $latest = Article::getLatest();
    $categories = Category::getAll();
    $tags = Tag::getAll();
    $authors = User::getAll();
    $data = Category::getArticlesByCategory($id, $itemsOnPage);
    $articles = $data['articles'];
    $pagination = $data['pagination'];

  	return $this->render('result', compact(
      'articles', 'pagination', 'latest',
      'categories', 'tags', 'authors'));
  }

  public function actionTag($id) {
    $itemsOnPage = 3;
    $latest = Article::getLatest();
    $categories = Category::getAll();
    $tags = Tag::getAll();
    $authors = User::getAll();
    $data = Tag::getArticlesByTag($id, $itemsOnPage);
    $articles = $data['articles'];
    $pagination = $data['pagination'];

    return $this->render('result', compact(
      'articles', 'pagination', 'latest',
      'categories', 'tags', 'authors'));
  }

  public function actionSearch($searchQuery) {
      $limit = 10;
      $data = SiteSearch::search($searchQuery, $limit);
      $articles = $data['articles'];
      $users = $data['users'];
      $comments = $data['comments'];
      $latest = Article::getLatest();
      $categories = Category::getAll();
      $tags = Tag::getAll();
      $authors = User::getAll();

      return $this->render('search_result', compact(
        'articles', 'users', 'comments',
        'latest', 'categories', 'tags', 'authors'));
  }

  public function actionUserArticles($id) {
    $itemsOnPage = 3;
    $latest = Article::getLatest();
    $categories = Category::getAll();
    $tags = Tag::getAll();
    $authors = User::getAll();
    $data = User::getAllArticlesByUser($id, $itemsOnPage);
    $articles = $data['articles'];
    $pagination = $data['pagination'];
    
    return $this->render('result', compact(
      'latest', 'categories', 'tags',
      'authors', 'articles', 'pagination'));
  }

}