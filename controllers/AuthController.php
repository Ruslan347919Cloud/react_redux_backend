<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\User;
use app\models\LoginForm;
use app\models\SignupForm;

class AuthController extends Controller {
	
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup() {
    	$signupForm = new SignupForm;
    	if (Yii::$app->request->isPost) {
    		$signupForm->load(Yii::$app->request->post());
    		if ($signupForm->signup()) {
                Yii::$app->session->setFlash('success', 'Check your email to finish your registration.');
                return $this->redirect(['site/index']);
            }		
    	}
    	return $this->render('signup', compact('signupForm'));
    }

    public function actionSignupConfirm($token) {
        if (empty($token)) {
            throw new \DomainException('Empty confirm token.');
        }

        $user = User::findOne(['email_confirm_token' => $token]);
        if (!$user) {
            throw new \DomainException('User is not found.');
        }
        $user->email_confirm_token = null;
        $user->save(false);

        if (!Yii::$app->getUser()->login($user)){
            throw new \RuntimeException('Error authentication.');
        }

        Yii::$app->session->setFlash('success', 'You have successfully confirmed your registration.');
        return $this->goHome();
    }
}