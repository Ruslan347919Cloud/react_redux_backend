<?php
namespace app\controllers;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE');
header('Content-type: application/json');

use Yii;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\Article;
use app\models\Category;
use app\models\Tag;
use app\models\User;
use app\models\Comment;
use app\models\SiteSearch;
use app\models\PaginationReact;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class SiteRestController extends Controller {

	public function behaviors() {
      return [
          'access' => [
              'class' => AccessControl::className(),
              'only' => ['logout'],
              'rules' => [
                  [
                      'actions' => ['logout'],
                      'allow' => true,
                      'roles' => ['@'],
                  ],
              ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'logout' => ['post'],
              ],
          ],
      ];
  }
  /**
   * {@inheritdoc}
   */
  public function actions()
  {
      return [
          'error' => [
              'class' => 'yii\web\ErrorAction',
          ],
          'captcha' => [
              'class' => 'yii\captcha\CaptchaAction',
              'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
          ],
      ];
  }

  //for React
  public function actionAllArticles($page, $limit = 3) {
    $data = Article::selectAllForJson($page, $limit);
    $articles = $data['articles'];
    $pages = $data['pages'];
    return $this->asJson(compact('articles', 'pages'));
  }

  public function actionSidebar() {
    $latest = Article::getLatest();
    $categoriesQuery = Category::getAll();
    $categories = [];
    foreach ($categoriesQuery as $category)
      $categories[] = [
        'id' => $category->id,
        'title' => $category->title,
        'articlesCount' => (int)$category->getArticlesCount()
      ];
    $tags = Tag::getAllAsArray();
    $authorsQuery = User::getAll();
    $authors = [];
    foreach ($authorsQuery as $author)
      $authors[] = [
        'id' => $author->id,
        'name' => $author->name,
        'email' => $author->email,
        'photo' => $author->photo,
        'articlesCount' => (int)$author->getArticlesCount()
      ];
    $data = compact('latest', 'categories', 'tags', 'authors');
    return $this->asJson($data);
  }

  public function actionSingleArticle($id) {
    $articleQuery = Article::selectOne($id);
    $articleQuery->updateCounters(['viewed' => 1]);
    $article = ArrayHelper::toArray($articleQuery);
    ArrayHelper::setValue($article, 'username', $articleQuery->user->name);
    return $this->asJson(compact('article'));
  }

  public function actionArticleComments($id, $page, $limit) {
    /*
    $commentModel = new Comment;
    */
    $commentsData = Comment::getAllByArticleForJson($id, $page, $limit);
    $comments = $commentsData['comments'];
    $pages = $commentsData['pages'];
    return $this->asJson(compact('comments', 'pages'));
  }

  public function actionCategoryArticles($id, $page, $limit) {
    $data = Category::getArticlesByCategoryForJson($id, $page, $limit);
    $articles = $data['articles'];
    $pages = $data['pages'];
    return $this->asJson(compact('articles', 'pages'));
  }

  public function actionTagArticles($id, $page, $limit) {
    $data = Tag::getArticlesByTagForJson($id, $page, $limit);
    $articles = $data['articles'];
    $pages = $data['pages'];
    return $this->asJson(compact('articles', 'pages'));
  }

  public function actionUserArticles($id, $page, $limit) {
    $data = User::getArticlesByUserForJson($id, $page, $limit);
    $articles = $data['articles'];
    $pages = $data['pages'];
    return $this->asJson(compact('articles', 'pages'));
  }

  public function actionSearch($searchQuery) {
    $limit = 10;
    $data = SiteSearch::searchAsArray($searchQuery, $limit);
    return $this->asJson($data);
  }
}

//При перезагрузке страницы возвращается на index.php