<?php
namespace app\controllers;

header('Access-Control-Allow-Origin: http://localhost:3000');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE');
header('Content-type: application/json');

use Yii;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\auth\HttpBearerAuth;
use yii\web\Response;
use app\models\User;
use app\models\LoginForm;
use app\models\SignupForm;

class AuthRestController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => ['dashboard'],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['dashboard'],
            'rules' => [
                [
                    'actions' => ['dashboard'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        return $behaviors;
    }
	
    public function actionLogin() {
        /*if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);*/
        $request = Yii::$app->getRequest();
        $data = json_decode($request->bodyParams['json']);
        $model = new LoginForm();
        if ($model->load([
            'email' => $data->email,
            'password' => $data->password,
            'rememberMe' => $data->rememberMe
        ], '') && $model->login()) {
            return $this->asJson(['access_token' => Yii::$app->user->identity->getAuthKey()]);
        }
        else {
            $model->validate();
            return $this->asJson($model);
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup() {
    	/*$signupForm = new SignupForm;
    	if (Yii::$app->request->isPost) {
    		$signupForm->load(Yii::$app->request->post());
    		if ($signupForm->signup()) {
                Yii::$app->session->setFlash('success', 'Check your email to finish your registration.');
                return $this->redirect(['site/index']);
            }		
    	}
    	return $this->render('signup', compact('signupForm'));*/
        return $this->asJson('success!');
    }

    public function actionSignupConfirm($token) {
        if (empty($token)) {
            throw new \DomainException('Empty confirm token.');
        }

        $user = User::findOne(['email_confirm_token' => $token]);
        if (!$user) {
            throw new \DomainException('User is not found.');
        }
        $user->email_confirm_token = null;
        $user->save(false);

        if (!Yii::$app->getUser()->login($user)){
            throw new \RuntimeException('Error authentication.');
        }

        Yii::$app->session->setFlash('success', 'You have successfully confirmed your registration.');
        return $this->goHome();
    }

    public function actionDashboard() {
        $response = [
            'username' => Yii::$app->user->identity->username,
            'access_token' => Yii::$app->user->identity->getAuthKey(),
        ];
        return $response;
    }

    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                $response = [
                    'flash' => [
                        'class' => 'success',
                        'message' => 'Thank you for contacting us. We will respond to you as soon as possible.',
                    ]
                ];
            } else {
                $response = [
                    'flash' => [
                        'class' => 'error',
                        'message' => 'There was an error sending email.',
                    ]
                ];
            }
            return $response;
        } else {
            $model->validate();
            return $model;
        }
    }
}