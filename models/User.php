<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\data\Pagination;
use app\models\PaginationReact;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $photo
 * @property string $auth_key
 * @property string $email_confirm_token
 *
 * @property Article[] $articles
 * @property Comment[] $comments
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'password'], 'required'],
            [['name', 'email', 'password', 'photo', 'auth_key', 'email_confirm_token'], 'string', 'max' => 255],
            [['auth_key'], 'unique'],
            [['email_confirm_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'E-mail',
            'password' => 'Password',
            'photo' => 'Photo',
            'auth_key' => 'Auth Key',
            'email_confirm_token' => 'E-mail Confirm Token',
        ];
    }

    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(['access_token' => $token]);
    }

    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    public static function findByEmail($email) {
        return self::find()->where(['email' => $email])->one();
    }

    public static function findByUsername($username) {
        return static::findOne(['name' => $username]);
    }

    public function validatePassword($password) {
        return ($this->password === $password) ? true : false;
    }

    public function create() {
        return $this->save(false);
    }

    private function getUserRoles() {
        return Yii::$app->authManager->getRolesByUser($this->id);
    }

    private function assignDefaultRole() {
        if (count($this->getUserRoles()) === 0) {
            $auth = Yii::$app->authManager;
            $author = $auth->getRole('author');
            $auth->assign($author, $this->id);
            return true;
        }
        return false;
    }

    private function deleteUserRoles() {
        $auth = Yii::$app->authManager;
        $auth->revokeAll($this->id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['user_id' => 'id']);
    }

    public static function getAll() {
        return self::find()
            ->limit(3)->all();
    }

    public function getArticlesCount() {
        return $this->getArticles()->asArray()->count();
    }

    private function deleteUserArticles() {
        $userArticles = $this->articles;
        foreach ($userArticles as $article)
            $article->delete();
    }

    public function saveImage($fileName) {
        $this->photo = $fileName;
        return $this->save(false);
    }

    public function getImage() {
        return ($this->photo) ? Url::base(true).'/uploads/'.$this->photo : Url::base(true).'/no_image.jpg';
    }

    public function deleteImage() {
        $imageUploadModel = new ImageUpload();
        $imageUploadModel->deleteCurrentImage($this->photo);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $this->assignDefaultRole();
    }

    public function beforeDelete() {
        if (!parent::beforeDelete())
            return false;
        $this->deleteImage();
        $this->deleteUserRoles();
        $this->deleteUserArticles();
        return true;
    }

    public function sendMail($view, $subject, $params = []) {
        $mailer = Yii::$app->mailer;
        $mailer->getView()->params['name'] = $this->name;
        $result = $mailer->compose([
            'html' => 'views/'.$view.'-html',
            'text' => 'views/'.$view.'-text',
        ], $params)->setTo([$this->email => $this->name])
            ->setSubject($subject)
            ->send();
        $mailer->getView()->params['name'] = null;
        return $result;
    }

    public static function getAllArticlesByUser($id, $pageSize = 3)
    {
        $query = Article::find()
          ->joinWith('user')
          ->where(['user.id' => $id, 'visible' => true]);
        $pagination = new Pagination([
          'defaultPageSize' => $pageSize,
          'totalCount' => $query->count()
        ]);
        $articles = $query
          ->orderBy(['last_update' => SORT_DESC])
          ->offset($pagination->offset)
          ->limit($pagination->limit)->all();
        return compact('articles', 'pagination');
    }

    public static function getArticlesByUserForJson($id, $page, $limit) {
        $query = Article::find()
          ->joinWith('user')
          ->where(['user.id' => $id, 'visible' => true]);
        $total = (int)$query->count();

        $data = PaginationReact::pagination($page, $limit, $total);
        $pages = $data['pages'];
        $beginIndex = $data['beginIndex'];

        $articles = $query
          ->offset($beginIndex)
          ->limit($limit)
          ->asArray()->all();
        return compact('articles', 'pages');
    }

    public static function getAllForJson($page, $limit) {
        $query = User::find();
        $total = (int)$query->count();

        $data = PaginationReact::pagination($page, $limit, $total);
        $pages = $data['pages'];
        $beginIndex = $data['beginIndex'];

        $users = $query
          ->offset($beginIndex)
          ->limit($limit)
          ->asArray()->all();
        return compact('users', 'pages');
    }    
}
