<?php
namespace app\models;

use yii\base\Model;

class SiteSearch extends Model {
	public $searchQuery;

	public function rules() {
		return [
			[['searchQuery'], 'string'],
			[['searchQuery'], 'required']
		];
	}

	public static function search($searchQuery, $limit = 10) {
		$query = trim($searchQuery);
    $articles = Article::find()
      ->orWhere(['like', 'content', $query])
      ->orWhere(['like', 'title', $query])
      ->orWhere(['like', 'description', $query])
      ->with('user')
      ->limit($limit)
      ->all();
    $users = User::find()
      ->where(['like', 'name', $query])
      ->limit($limit)
      ->all();
    $comments = Comment::find()
      ->where(['like', 'text', $query])
      ->with('user', 'article')
      ->limit($limit)
      ->all();
    return compact('articles', 'users', 'comments');
	}

  public static function searchAsArray($searchQuery, $limit = 10) {
    $query = trim($searchQuery);
    $articles = Article::find()
      ->orWhere(['like', 'content', $query])
      ->orWhere(['like', 'title', $query])
      ->orWhere(['like', 'description', $query])
      ->with('user')
      ->limit($limit)
      ->asArray()->all();
    $users = User::find()
      ->where(['like', 'name', $query])
      ->limit($limit)
      ->asArray()->all();
    $comments = Comment::find()
      ->where(['like', 'text', $query])
      ->with('user', 'article')
      ->limit($limit)
      ->asArray()->all();
    return compact('articles', 'users', 'comments');
  }
}