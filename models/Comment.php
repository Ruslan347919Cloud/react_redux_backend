<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use app\models\PaginationReact;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property string $text
 * @property int $user_id
 * @property int $article_id
 * @property int $visible
 * @property string $put_date
 *
 * @property Article $article
 * @property User $user
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'article_id'], 'integer'],
            [['visible'], 'boolean'],
            [['text'], 'required'],
            [['text'], 'trim'],
            [['text'], 'string', 'max' => 3000],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['visible'], 'default', 'value' => false],
            [['put_date'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['put_date'], 'default', 'value' => date('Y-m-d H:i:s')]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
            'user_id' => 'User ID',
            'article_id' => 'Article ID',
            'visible' => 'Visible',
            'put_date' => 'Put Date',
        ];
    }

    public function __construct($articleId = null) {
        parent::init();
        $this->article_id = $articleId;
        $this->user_id = Yii::$app->user->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getAllForArticle($id, $pageSize = 3) {
        $query = self::find()
            ->where(['article_id' => $id, 'visible' => true]);
        $pagination = new Pagination([
            'defaultPageSize' => $pageSize,
            'totalCount' => $query->count()
        ]);
        $comments = $query->with('user')
            ->orderBy(['id' => SORT_DESC])
            ->offset($pagination->offset)
            ->limit($pagination->limit)->all();
        return compact('comments', 'pagination');
    }

    public static function getAllByArticleForJson($id, $page, $limit) {
        $query = Comment::find()
            ->where(['article_id' => $id, 'visible' => true]);
        $total = (int)$query->count();

        $data = PaginationReact::pagination($page, $limit, $total);
        $pages = $data['pages'];
        $beginIndex = $data['beginIndex'];

        $comments = $query->with('user')
            ->orderBy(['id' => SORT_DESC])
            ->offset($beginIndex)
            ->limit($limit)
            ->asArray()->all();
        return compact('comments', 'pages');
    }

    public static function getAllforAdminJson($page, $limit = 1) {
        $query = self::find();
        $total = (int)$query->count();

        $pagination = PaginationReact::pagination($page, $limit, $total);
        $pages = $pagination['pages'];
        $beginIndex = $pagination['beginIndex'];

        $comments = $query
          ->offset($beginIndex)
          ->limit($limit)
          ->asArray()->all();
        return compact('comments', 'pages');
    }

    public function getDate() {
        return Yii::$app->formatter->asDate($this->put_date);
    }

    public function changeStatus() {
        $this->visible = !$this->visible;
        return $this->save(false);
    }
}
