<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\ImageUpload;
use app\models\PaginationReact;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $put_date
 * @property string $last_update
 * @property string $image
 * @property int $viewed
 * @property int $user_id
 * @property int $visible
 *
 * @property User $user
 * @property ArticleCategory[] $articleCategories
 * @property Category[] $categories
 * @property ArticleTag[] $articleTags
 * @property Tag[] $tags
 * @property Comment[] $comments
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content'], 'string'],
            [['title', 'description', 'content'], 'trim'],
            [['put_date', 'last_update'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['put_date', 'last_update'], 'default', 'value' => date('Y-m-d H:i:s')],
            [['viewed', 'user_id'], 'integer'],
            [['visible'], 'boolean'],
            [['visible'], 'default', 'value' => false],
            [['title', 'description', 'image'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'content' => 'Content',
            'put_date' => 'Put Date',
            'last_update' => 'Last Update',
            'image' => 'Image',
            'viewed' => 'Viewed',
            'user_id' => 'User ID',
            'visible' => 'Visible',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategories()
    {
        return $this->hasMany(ArticleCategory::className(), ['article_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('article_category', ['article_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleTags()
    {
        return $this->hasMany(ArticleTag::className(), ['article_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('article_tag', ['article_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['article_id' => 'id']);
    }

    public static function selectOne($id) {
        return self::find()
            ->with('user')
            ->where(['id' => $id])->one();
    }

    public static function selectAll($pageSize = 3) {
        $query = self::find()
            ->where(['visible' => true]);
        $pagination = new Pagination([
            'defaultPageSize' => $pageSize,
            'totalCount' => $query->count()
        ]);
        $articles = $query->with('user')
            ->orderBy(['last_update' => SORT_DESC])
            ->offset($pagination->offset)
            ->limit($pagination->limit)->all();
        return compact('articles', 'pagination');
    }

    public static function selectAllForJson($page, $limit = 1) {
        $query = Article::find()
          ->where(['visible' => true]);
        $total = (int)$query->count();

        $pagination = PaginationReact::pagination($page, $limit, $total);
        $pages = $pagination['pages'];
        $beginIndex = $pagination['beginIndex'];

        $articles = $query->with('user')
          ->orderBy(['last_update' => SORT_DESC])
          ->offset($beginIndex)
          ->limit($limit)
          ->asArray()->all();
        return compact('articles', 'pages');
    }

    public static function getAllForAdminJson($page, $limit = 1) {
        $query = self::find();
        $total = (int)$query->count();

        $pagination = PaginationReact::pagination($page, $limit, $total);
        $pages = $pagination['pages'];
        $beginIndex = $pagination['beginIndex'];

        $articles = $query
            //->orderBy(['last_update' => SORT_DESC])
            ->offset($beginIndex)
            ->limit($limit)
            ->asArray()->all();
        return compact('articles', 'pages');
    }

    public static function getLatest() {
        return self::find()
            ->asArray()
            ->orderBy(['put_date' => SORT_DESC])
            ->limit(3)->all();
    }

    public function getLastUpdateTime() {
        return Yii::$app->formatter
            ->asRelativeTime($this->last_update);
    }

    public function getDate() {
        return Yii::$app->formatter->asDate($this->put_date);
    }

    public function saveImage($fileName) {
        $this->image = $fileName;
        return $this->save(false);
    }

    public function getImage() {
        return ($this->image) ? Url::base(true).'/uploads/'.$this->image : Url::base(true).'/no_image.jpg';
    }

    public function deleteImage() {
        $imageUploadModel = new imageUpload();
        $imageUploadModel->deleteCurrentImage($this->image);
    }

    public function beforeDelete() {
        $this->deleteImage();
        return parent::beforeDelete();
    }

    public function getSelectedCategories() {
        $selectedIds = $this->getCategories()
            ->select('id')->asArray()->all();
        return ArrayHelper::getColumn($selectedIds, 'id');
    }

    private function clearCurrentCategories() {
        ArticleCategory::deleteAll(['article_id' => $this->id]);
    }

    public function saveCategories($categories) {
        if (is_array($categories)) {
            $this->clearCurrentCategories();
            foreach ($categories as $category_id) {
                $category = Category::findOne($category_id);
                $this->link('categories', $category);
            }
        }
    }

    public function getSelectedTags() {
        $selectedIds = $this->getTags()
            ->select('id')->asArray()->all();
        return ArrayHelper::getColumn($selectedIds, 'id');
    }

    private function clearCurrentTags() {
        ArticleTag::deleteAll(['article_id' => $this->id]);
    }

    public function saveTags($tags) {
        if (is_array($tags)) {
            $this->clearCurrentTags();
            foreach ($tags as $tag_id) {
                $tag = Tag::findOne($tag_id);
                $this->link('tags', $tag);
            }
        }
    }

    public function changeStatus() {
        $this->visible = !$this->visible;
        return $this->save(false);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->last_update = date('Y-m-d H:i:s');
            Yii::$app->session->setFlash('articleCreated', 'Your article was sent to server. It will appear on the main page soon.');
            return true;
        }
        return false;
    }
}
