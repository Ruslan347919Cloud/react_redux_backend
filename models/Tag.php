<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use app\models\PaginationReact;

/**
 * This is the model class for table "tag".
 *
 * @property int $id
 * @property string $title
 *
 * @property ArticleTag[] $articleTags
 * @property Article[] $articles
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleTags()
    {
        return $this->hasMany(ArticleTag::className(), ['tag_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['id' => 'article_id'])->viaTable('article_tag', ['tag_id' => 'id']);
    }

    public static function getAll() {
        return self::find()->all();
    }

    public static function getAllAsArray() {
        return self::find()->asArray()->all();
    }

    public static function getArticlesByTag($id, $pageSize = 3) {
        $query = Article::find()
            ->joinWith('tags')
            ->where(['tag.id' => $id]);
        $pagination = new Pagination([
            'defaultPageSize' => $pageSize,
            'totalCount' => $query->count()
        ]);
        $articles = $query
            ->offset($pagination->offset)
            ->limit($pagination->limit)->all();
        return compact('articles', 'pagination');
    }

    public static function getArticlesByTagForJson($id, $page, $limit) {
        $query = Article::find()
          ->joinWith('tags')
          ->where(['tag.id' => $id]);
        $total = (int)$query->count();

        $data = PaginationReact::pagination($page, $limit, $total);
        $pages = $data['pages'];
        $beginIndex = $data['beginIndex'];

        $articles = $query
          ->offset($beginIndex)
          ->limit($limit)
          ->asArray()->all();
        return compact('articles', 'pages');
    }

    public static function getAllforAdminJson($page, $limit = 1) {
        $query = self::find();
        $total = (int)$query->count();

        $pagination = PaginationReact::pagination($page, $limit, $total);
        $pages = $pagination['pages'];
        $beginIndex = $pagination['beginIndex'];

        $tags = $query
          ->offset($beginIndex)
          ->limit($limit)
          ->asArray()->all();
        return compact('tags', 'pages');
    }
}
