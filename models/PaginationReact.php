<?php
namespace app\models;

use Yii;
use yii\base\Model;

class PaginationReact extends Model {

	public static function pagination($page, $pageSize = 1, $itemsTotal = 1) {

    $pagesTotal = ceil($itemsTotal/$pageSize);
    $page = ($page > $pagesTotal) ? $pagesTotal : $page;
    $pages = [];
    for ($i = 0; $i < $pagesTotal; $i++) {
      $pages[] = [
        'number' => $i+1,
        'active' => ((int)$page === (int)($i+1))
      ];
    }
    $beginIndex = ($page-1) * $pageSize;

    return compact('pages', 'beginIndex');
	}

}