<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ImageUpload extends Model {
	public $image;

	public function rules() {
		return [
			[['image'], 'required'],
			[['image'], 'file', 'extensions' => 'jpg,png,gif']
		];
	}

	private function getFolder() {
		return Yii::getAlias('@web').'uploads/';
	}

	private function generateFilename() {
		return strtolower(md5(uniqid($this->image->baseName)).'.'.$this->image->extension);
	}

	private function fileExists($currentImage) {
		if (!empty($currentImage) && $currentImage !== NULL) {
			return file_exists($this->getFolder().$currentImage);
		}
	}

	public function deleteCurrentImage($currentImage) {
		if ($this->fileExists($currentImage))
				unlink($this->getFolder().$currentImage);
	}

	private function saveImage() {
		$fileName = $this->generateFilename();
		$this->image->saveAs($this->getFolder().$fileName);
		return $fileName;
	}

	public function uploadFile(UploadedFile $file, $currentImage) {
		$this->image = $file;
		if ($this->validate()) {
			$this->deleteCurrentImage($currentImage);
			return $this->saveImage();
		}
	}
}