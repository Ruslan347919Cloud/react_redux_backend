<?php

namespace app\models;

use Yii;
use app\models\ArticleCategory;
use yii\data\Pagination;
use app\models\PaginationReact;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $title
 *
 * @property ArticleCategory[] $articleCategories
 * @property Article[] $articles
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategories()
    {
        return $this->hasMany(ArticleCategory::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['id' => 'article_id'])->viaTable('article_category', ['category_id' => 'id']);
    }

    public static function getAll() {
        return self::find()->all();
    }

    public function getArticlesCount() {
        return ArticleCategory::find()
            ->asArray()
            ->where(['category_id' => $this->id])
            ->count();
    }

    public static function getArticlesByCategory($id, $pageSize = 3) {
        $query = Article::find()
            ->joinWith('categories')
            ->where(['category.id' => $id]);
        $pagination = new Pagination([
            'defaultPageSize' => $pageSize,
            'totalCount' => $query->count()
        ]);
        $articles = $query
            ->offset($pagination->offset)
            ->limit($pagination->limit)->all();
        return compact('articles', 'pagination');
    }

    public static function getArticlesByCategoryForJson($id, $page, $limit) {
        $query = Article::find()
          ->joinWith('categories')
          ->where(['category.id' => $id]);
        $total = (int)$query->count();

        $data = PaginationReact::pagination($page, $limit, $total);
        $pages = $data['pages'];
        $beginIndex = $data['beginIndex'];

        $articles = $query
          ->offset($beginIndex)
          ->limit($limit)
          ->asArray()->all();
        return compact('articles', 'pages');
    }

    public static function getAllForJson($page, $limit) {
        $query = Category::find();
        $total = (int)$query->count();

        $data = PaginationReact::pagination($page, $limit, $total);
        $pages = $data['pages'];
        $beginIndex = $data['beginIndex'];

        $categories = $query
          ->offset($beginIndex)
          ->limit($limit)
          ->asArray()->all();
        return compact('categories', 'pages');
    }
}
