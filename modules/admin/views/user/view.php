<?php

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'format' => 'html',
                'label' => 'Photo',
                'value' => function($data) {
                    return Html::img($data->getImage(), ['width' => 150]);
                }
            ],
            'id',
            'name',
            'email:email',
            [
                'format' => 'html',
                'label' => 'Password',
                'value' => function($data) {
                    $password = (Yii::$app->user->can(
                        'updateUser', ['user' => $data])) ? $data->password : 'Hidden';
                    return $password;
                }
            ],
        ],
    ]) ?>

        <?php if (Yii::$app->user->can('updateUser', ['user' => $model])):?>
            <p>
                <?= Html::a('Create article', ['/admin/article/create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                <?= Html::a('Update user info', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Set user photo', ['set-photo', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete user', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?php if ($model->articles):?>
            <h3>My Articles</h3>
            <ul>
                <?php foreach ($model->articles as $article):?>
                <li>
                    <a href="<?=Url::to([
                            '/admin/article/view',
                            'id' => $article->id
                        ])?>">
                        <?=Html::encode($article->title)?>
                    </a>
                </li>
                <?php endforeach;?>
            </ul>
            <?php else:?>
                <p>This user have no articles.</p>
            <?php endif;?>
        <?php else:?>
            <div>
                <a href="<?=Url::to(['/site/user-articles', 'id' => $model->id])?>">Find articles by this user</a>
            </div>
        <?php endif;?>

</div>
