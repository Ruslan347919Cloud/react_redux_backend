<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Article', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description',
            [
                'attribute' => 'content',
                'format' => 'html',
                'label' => 'Content',
                'value' => function($data) {
                    return \yii\helpers\StringHelper::truncate(
                        $data->content, 200);
                }
            ],
            'put_date',
            'last_update',
            [
                'format' => 'html',
                'label' => 'Image',
                'value' => function($data) {
                    return Html::img($data->getImage(), ['width' => 150]);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'headerOptions' => ['width' => '95'],
                'template' => '{change-status} {view} {update} {delete}',
                'buttons' => [
                    'change-status' => function ($url, $model) {
                        $allowIcon = '<i class=\'fa fa-check-circle\' aria-hidden=\'true\'></i>';
                        $disallowIcon = '<i class=\'fa fa-ban\' aria-hidden=\'true\'></i>';
                        $commentStatus = $model->visible;
                        $icon = ($commentStatus) ? $disallowIcon : $allowIcon;
                        $title = ($commentStatus) ? 'Disallow' : 'Allow';
                        return Html::a($icon, $url, compact('title'));
                    },
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', $url, [
                            'title' => 'View',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil" aria-hidden="true"></i>', $url, [
                            'title' => 'Update',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash-o" aria-hidden="true"></i>', $url, [
                            'title' => 'Delete',
                            'data' => [
                                'method' => 'post',
                                'confirm' =>'Are you sure you want to delete this item?',
                            ]
                        ]);
                    },
                ],
                'visibleButtons' =>
                [
                    'change-status' => function ($model) {
                        return Yii::$app->user->can('updateArticle', ['article' => $model]);
                    },
                    'view' => function ($model) {
                        return Yii::$app->user->can('manageArticle', ['article' => $model]);
                    },
                    'update' => function ($model) {
                        return Yii::$app->user->can('updateArticle', ['article' => $model]);
                    },
                    'delete' => function ($model) {
                        return Yii::$app->user->can('deleteArticle', ['article' => $model]);
                    },
                ]
            ],
        ],
    ]); ?>


</div>
