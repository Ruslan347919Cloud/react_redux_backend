<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
?>

<div class="article-form">
	<?php $form = ActiveForm::begin();?>
	<?=$form->field($imageUpload, 'image')->fileInput()?>
	<div class="form-group">
		<?=Html::submitButton('Submit', ['class' => 'btn btn-success'])?>
	</div>
	<?php ActiveForm::end();?>
</div>