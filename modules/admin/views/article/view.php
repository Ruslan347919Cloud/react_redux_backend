<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('articleCreated')):?>
        <div class="alert alert-success alert-dismissible col-12" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <?php echo Yii::$app->session->getFlash('articleCreated'); ?>
        </div>
    <?php endif;?>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Set image', ['set-image', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if (Yii::$app->user->can('admin')):?>
            <?= Html::a('Set categories', ['set-categories', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Set tags', ['set-tags', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif;?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description',
            'content:ntext',
            'put_date',
            'last_update',
            [
                'format' => 'html',
                'label' => 'Image',
                'value' => function($data) {
                    return Html::img($data->getImage(), ['width' => 300]);
                }
            ],
            'viewed',
            [
                'format' => 'html',
                'label' => 'Author',
                'value' => function($data) {
                    return ($data->user) ? $data->user->name.' (ID: '.$data->user->id.')' : null;
                }
            ],
            'visible',
        ],
    ]) ?>

    <?php if ($model->categories):?>
        <h4>Related categories</h4>
        <ul>
            <?php foreach ($model->categories as $category):?>
                <li>
                    <?=Html::encode($category->title)?>
                </li>
            <?php endforeach;?>
        </ul>
    <?php else:?>
        <h4>No categories</h4>
    <?php endif;?>
    
    <?php if ($model->tags):?>
        <h4>Related tags</h4>
        <ul>
            <?php foreach ($model->tags as $tag):?>
                <li>
                    <?=Html::encode($tag->title)?>
                </li>
            <?php endforeach;?>
        </ul>
    <?php else:?>
        <h4>No tags</h4>
    <?php endif;?>


</div>
