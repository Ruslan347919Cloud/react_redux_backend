<?php

namespace app\modules\admin\controllers;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE');
header('Content-type: application/json');

use Yii;
use app\models\Comment;
use app\models\CommentSearch;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CommentController implements the CRUD actions for Comment model.
 */
class CommentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        //'roles' => ['manageComment']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        //'roles' => ['viewComment']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        //'roles' => ['createComment']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        //'roles' => ['updateComment']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        //'roles' => ['deleteComment']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-status'],
                        //'roles' => ['updateComment']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Comment models.
     * @return mixed
     */
    public function actionIndex($page, $limit) {
        $data = Comment::getAllforAdminJson($page, $limit);
        $items = $data['comments'];
        $pages = $data['pages'];
        return $this->asJson(compact('items', 'pages'));
    }

    /**
     * Displays a single Comment model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $comment = Comment::find()
            ->where(['id' => $id])
            ->with('article', 'user')
            ->asArray()->one();
        return $this->asJson($comment);
    }

    /**
     * Creates a new Comment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->getRequest();
        $data = json_decode($request->bodyParams['json']);
        $model = new Comment();
        $model->load([
            'text' => $data->text,
            'user_id' => $data->user_id,
            'article_id' => $data->article_id
        ], '');
        $model->save();

        return $this->asJson($model);
    }

    /**
     * Updates an existing Comment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id = null)
    {
        if (Yii::$app->request->isGet && $id !== null) {
            $comment = Comment::find()
                ->where(['id' => $id])
                ->asArray()->one();
            return $this->asJson($comment);
        } else {
            $request = Yii::$app->getRequest();
            $data = json_decode($request->bodyParams['json']);
            $model = $this->findModel($data->id);
            $model->load([
                'text' => $data->text,
                'user_id' => $data->user_id,
                'article_id' => $data->article_id
            ], '');
            $model->save();
            return $this->asJson($model);
        }
    }

    /**
     * Deletes an existing Comment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete() {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        $model->delete();
        return $this->asJson($id);
    }

    /**
     * Finds the Comment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Comment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Comment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionChangeStatus($id) {
        $currentComment = $this->findModel($id);
        $currentComment->changeStatus();
        return $this->asJson($currentComment->visible);
    }
}
