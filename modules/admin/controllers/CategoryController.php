<?php

namespace app\modules\admin\controllers;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE');
header('Content-type: application/json');

use Yii;
use app\models\Category;
use app\models\CategorySearch;
use app\models\PaginationReact;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        //'roles' => ['manageCategory']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        //'roles' => ['viewCategory']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        //'roles' => ['createCategory']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        //'roles' => ['updateCategory']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        //'roles' => ['deleteCategory']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex($page, $limit) {
        $data = Category::getAllForJson($page, $limit);
        $items = $data['categories'];
        $pages = $data['pages'];
        return $this->asJson(compact('items', 'pages'));
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $page, $limit=3) {
        $data = Category::getArticlesByCategoryForJson($id, $page, $limit);
        $articles = $data['articles'];
        $pages = $data['pages'];
        $category = Category::find()
            ->where(['id' => $id])
            ->asArray()->one();
        return $this->asJson(compact('category', 'articles', 'pages'));
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->getRequest();
        $data = json_decode($request->bodyParams['json']);
        $model = new Category();
        $model->load([
            'title' => $data->title
        ], '');
        $model->save();

        return $this->asJson($model);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id = null)
    {
        if (Yii::$app->request->isGet && $id !== null) {
            $category = Category::find()
                ->where(['id' => $id])
                ->asArray()->one();
            return $this->asJson($category);
        } else {
            $request = Yii::$app->getRequest();
            $data = json_decode($request->bodyParams['json']);
            $model = $this->findModel($data->id);
            $model->load([
                'title' => $data->title
            ], '');
            $model->save();
            return $this->asJson($model);
        }
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete() {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        $model->delete();
        return $this->asJson($id);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
