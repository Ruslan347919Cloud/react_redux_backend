<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use app\models\ImageUpload;
use app\models\Category;
use app\models\Tag;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['manageArticle']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['createArticle']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteArticle']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['set-categories'],
                        'roles' => ['manageArticle']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['set-tags'],
                        'roles' => ['manageArticle']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['set-image'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-status'],
                        'roles' => ['manageArticle']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('updateArticle', ['article' => $model])) {
            throw new \yii\web\ForbiddenHttpException('Access denied');
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('updateArticle', ['article' => $model])) {
            throw new \yii\web\ForbiddenHttpException('Access denied');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSetCategories($id) {
        $article = $this->findModel($id);
        $selectedCategories = $article->getSelectedCategories();
        $categories = ArrayHelper::map(
            Category::find()->all(), 'id', 'title');
        if (Yii::$app->request->isPost) {
            $categories = Yii::$app->request->post('categories');
            $article->saveCategories($categories);
            return $this->redirect(['view', 'id' => $article->id]);
        }
        return $this->render('set_categories', compact('selectedCategories', 'categories'));
    }

    public function actionSetTags($id) {
        $article = $this->findModel($id);
        $selectedTags = $article->getSelectedTags();
        $tags = ArrayHelper::map(
            Tag::find()->all(), 'id', 'title');
        if (Yii::$app->request->isPost) {
            $tags = Yii::$app->request->post('tags');
            $article->saveTags($tags);
            return $this->redirect(['view', 'id' => $article->id]);
        }
        return $this->render('set_tags',
            compact('selectedTags', 'tags'));
    }

    public function actionSetImage($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('updateArticle', ['article' => $model])) {
            throw new \yii\web\ForbiddenHttpException('Access denied');
        }
        $imageUpload = new ImageUpload();
        if (Yii::$app->request->isPost) {
            $article = $this->findModel($id);
            $uploadedFile = UploadedFile::getInstance(
                $imageUpload, 'image');
            if ($article->saveImage($imageUpload->uploadFile($uploadedFile, $article->image))) {
                return $this->redirect(
                    ['view', 'id' => $article->id]);
            }
        }
        return $this->render('set_image', compact('imageUpload'));
    }

    public function actionChangeStatus($id) {
        $currentArticle = $this->findModel($id);
        $currentArticle->changeStatus();
        return $this->redirect(['article/index']);
    }
}
