<?php

namespace app\modules\admin\controllers;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE');
header('Content-type: application/json');

use Yii;
use app\models\Tag;
use app\models\TagSearch;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class TagController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        //'roles' => ['manageTag']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        //'roles' => ['viewTag']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        //'roles' => ['createTag']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        //'roles' => ['updateTag']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        //'roles' => ['deleteTag']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tag models.
     * @return mixed
     */
    public function actionIndex($page, $limit) {
        $data = Tag::getAllforAdminJson($page, $limit);
        $items = $data['tags'];
        $pages = $data['pages'];
        return $this->asJson(compact('items', 'pages'));
    }

    /**
     * Displays a single Tag model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $page, $limit=3) {
        $tag = Tag::find()
            ->where(['id' => $id])
            ->asArray()->one();
        $data = Tag::getArticlesByTagForJson($id, $page, $limit);
        $articles = $data['articles'];
        $pages = $data['pages'];
        return $this->asJson(compact('tag', 'articles', 'pages'));
    }

    /**
     * Creates a new Tag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->getRequest();
        $data = json_decode($request->bodyParams['json']);
        $model = new Tag();
        $model->load([
            'title' => $data->title
        ], '');
        $model->save();

        return $this->asJson($model);
    }

    /**
     * Updates an existing Tag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id = null)
    {
        if (Yii::$app->request->isGet && $id !== null) {
            $tag = Tag::find()
                ->where(['id' => $id])
                ->asArray()->one();
            return $this->asJson($tag);
        } else {
            $request = Yii::$app->getRequest();
            $data = json_decode($request->bodyParams['json']);
            $model = $this->findModel($data->id);
            $model->load([
                'title' => $data->title
            ], '');
            $model->save();
            return $this->asJson($model);
        }
    }

    /**
     * Deletes an existing Tag model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        $model->delete();
        return $this->asJson($id);
    }

    /**
     * Finds the Tag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tag::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
