<?php

namespace app\modules\admin\controllers;


header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE');
header('Content-type: application/json');

use Yii;
use app\models\User;
use app\models\ImageUpload;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        //'roles' => ['createUser']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['set-photo'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex($page, $limit) {
        $data = User::getAllForJson($page, $limit);
        $items = $data['users'];
        $pages = $data['pages'];
        return $this->asJson(compact('items', 'pages'));
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $page, $limit=3) {
        $data = User::getArticlesByUserForJson($id, $page, $limit);
        $articles = $data['articles'];
        $pages = $data['pages'];
        $user = User::find()
            ->where(['id' => $id])
            ->asArray()->one();
        return $this->asJson(compact('user', 'articles', 'pages'));
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->getRequest();
        $data = json_decode($request->bodyParams['json']);
        $model = new User();
        $model->load([
            'name' => $data->name,
            'email' => $data->email,
            'password' => $data->password
        ], '');
        $model->save();

        return $this->asJson($model);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id = null)
    {
        if (Yii::$app->request->isGet && $id !== null) {
            $user = User::find()
                ->where(['id' => $id])
                ->asArray()->one();
            return $this->asJson($user);
        } else {
            $request = Yii::$app->getRequest();
            $data = json_decode($request->bodyParams['json']);
            $model = $this->findModel($data->id);
            $model->load([
                'name' => $data->name,
                'email' => $data->email,
                'password' => $data->password
            ], '');
            $model->save();
            return $this->asJson($model);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        $model->delete();
        return $this->asJson($id);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSetPhoto($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('updateUser', ['user' => $model])) {
            throw new \yii\web\ForbiddenHttpException('Access denied');
        }
        $imageUpload = new ImageUpload;
        if (Yii::$app->request->isPost) {
            $user = $this->findModel($id);
            $uploadedFile = UploadedFile::getInstance(
                $imageUpload, 'image');
            if ($user->saveImage($imageUpload->uploadFile($uploadedFile, $user->photo))) {
                return $this->redirect(
                    ['view', 'id' => $user->id]);
            }
        }
        return $this->render('/article/set_image', compact('imageUpload'));
    }
}
