<?php

namespace app\modules\admin\controllers;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE');
header('Content-type: application/json');

use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use app\models\ImageUpload;
use app\models\Category;
use app\models\Tag;
use app\models\PaginationReact;
use app\models\Comment;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        //'roles' => ['manageArticle']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        //'roles' => ['createArticle']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        //'roles' => ['deleteArticle']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['set-categories'],
                        //'roles' => ['manageArticle']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['set-tags'],
                        //'roles' => ['manageArticle']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['set-image'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-status'],
                        //'roles' => ['manageArticle']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex($page, $limit) {
        $data = Article::getAllForAdminJson($page, $limit);
        $items = $data['articles'];
        $pages = $data['pages'];
        return $this->asJson(compact('items', 'pages'));
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $page, $limit=3) {
        $article = Article::find()
            ->where(['id' => $id])
            ->asArray()->one();
        $data = Comment::getAllByArticleForJson($id, $page, $limit);
        $comments = $data['comments'];
        $pages = $data['pages'];
        return $this->asJson(compact('article', 'comments', 'pages'));
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->getRequest();
        $data = json_decode($request->bodyParams['json']);
        $model = new Article();
        $model->load([
            'title' => $data->title,
            'description' => $data->description,
            'content' => $data->content,
            'user_id' => $data->user_id
        ], '');
        $model->save();

        return $this->asJson($model);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id = null)
    {
        if (Yii::$app->request->isGet && $id !== null) {
            $article = Article::find()
                ->where(['id' => $id])
                ->asArray()->one();
            return $this->asJson($article);
        } else {
            $request = Yii::$app->getRequest();
            $data = json_decode($request->bodyParams['json']);
            $model = $this->findModel($data->id);
            $model->load([
                'title' => $data->title,
                'description' => $data->description,
                'content' => $data->content,
                'user_id' => $data->user_id
            ], '');
            $model->save();
            return $this->asJson($model);
        }
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        $model->delete();
        return $this->asJson($id);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSetCategories() {
        /*$article = $this->findModel($id);
        $selectedCategories = $article->getSelectedCategories();
        $categories = ArrayHelper::map(
            Category::find()->all(), 'id', 'title');
        if (Yii::$app->request->isPost) {
            $categories = Yii::$app->request->post('categories');
            $article->saveCategories($categories);
            return $this->redirect(['view', 'id' => $article->id]);
        }
        return $this->render('set_categories', compact('selectedCategories', 'categories'));*/
        $request = Yii::$app->getRequest();
        $data = json_decode($request->bodyParams['json']);
        return $this->asJson($data);
    }

    public function actionSetTags() {
        /*$article = $this->findModel($id);
        $selectedTags = $article->getSelectedTags();
        $tags = ArrayHelper::map(
            Tag::find()->all(), 'id', 'title');
        if (Yii::$app->request->isPost) {
            $tags = Yii::$app->request->post('tags');
            $article->saveTags($tags);
            return $this->redirect(['view', 'id' => $article->id]);
        }
        return $this->render('set_tags',
            compact('selectedTags', 'tags'));*/
        $request = Yii::$app->getRequest();
        $data = json_decode($request->bodyParams['json']);
        return $this->asJson($data);
    }

    public function actionSetImage($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('updateArticle', ['article' => $model])) {
            throw new \yii\web\ForbiddenHttpException('Access denied');
        }
        $imageUpload = new ImageUpload();
        if (Yii::$app->request->isPost) {
            $article = $this->findModel($id);
            $uploadedFile = UploadedFile::getInstance(
                $imageUpload, 'image');
            if ($article->saveImage($imageUpload->uploadFile($uploadedFile, $article->image))) {
                return $this->redirect(
                    ['view', 'id' => $article->id]);
            }
        }
        return $this->render('set_image', compact('imageUpload'));
    }

    public function actionChangeStatus($id) {
        $currentArticle = $this->findModel($id);
        $currentArticle->changeStatus();
        return $this->asJson($currentArticle->visible);
    }
}
