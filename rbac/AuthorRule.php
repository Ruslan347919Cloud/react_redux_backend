<?php
namespace app\rbac;

use yii\rbac\Rule;

class AuthorRule extends Rule {
	public $name = 'isAuthor';

	public function execute($user, $items, $params) {
		return isset($params['article']) ? $params['article']->user_id === $user : false;
	}
}