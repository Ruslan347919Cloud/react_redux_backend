<?php
namespace app\rbac;

use yii\rbac\Rule;

class UserRule extends Rule {
	public $name = 'isUser';

	public function execute($user, $items, $params) {
		return isset($params['user']) ? $params['user']->id === $user : false;
	}
}