<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%article}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m191028_112341_create_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%article}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->string(),
            'content' => $this->text()->notNull(),
            'put_date' => $this->dateTime(),
            'last_update' => $this->dateTime(),
            'image' => $this->string(),
            'viewed' => $this->integer()->defaultValue(0),
            'user_id' => $this->integer(),
            'visible' => $this->boolean()->defaultValue(0),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-article-user_id}}',
            '{{%article}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-article-user_id}}',
            '{{%article}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-article-user_id}}',
            '{{%article}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-article-user_id}}',
            '{{%article}}'
        );

        $this->dropTable('{{%article}}');
    }
}
