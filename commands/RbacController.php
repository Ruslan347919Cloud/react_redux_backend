<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller {
    public function actionInit() {
        $auth = Yii::$app->authManager;
        
        $auth->removeAll();

        // Add rules
        $authorRule = new \app\rbac\AuthorRule;
        $auth->add($authorRule);
        $userRule = new \app\rbac\UserRule;
        $auth->add($userRule);

        // Permissions for Article
        $manageArticle = $auth->createPermission('manageArticle');
        $manageArticle->description = 'Manage Article';
        $auth->add($manageArticle);

        $viewArticle = $auth->createPermission('viewArticle');
        $viewArticle->description = 'View Article';
        $auth->add($viewArticle);
        
        $createArticle = $auth->createPermission('createArticle');
        $createArticle->description = 'Create a Article';
        $auth->add($createArticle);
        
        $updateArticle = $auth->createPermission('updateArticle');
        $updateArticle->description = 'Update Article';
        $auth->add($updateArticle);
        
        $deleteArticle = $auth->createPermission('deleteArticle');
        $deleteArticle->description = 'Delete Article';
        $auth->add($deleteArticle);

        // Permissions for User
        $manageUser = $auth->createPermission('manageUser');
        $manageUser->description = 'Manage User';
        $auth->add($manageUser);

        $viewUser = $auth->createPermission('viewUser');
        $viewUser->description = 'View User';
        $auth->add($viewUser);
        
        $createUser = $auth->createPermission('createUser');
        $createUser->description = 'Create a User';
        $auth->add($createUser);
        
        $updateUser = $auth->createPermission('updateUser');
        $updateUser->description = 'Update User';
        $auth->add($updateUser);
        
        $deleteUser = $auth->createPermission('deleteUser');
        $deleteUser->description = 'Delete User';
        $auth->add($deleteUser);

        // Permissions for Category
        $manageCategory = $auth->createPermission('manageCategory');
        $manageCategory->description = 'Manage Category';
        $auth->add($manageCategory);

        $viewCategory = $auth->createPermission('viewCategory');
        $viewCategory->description = 'View Category';
        $auth->add($viewCategory);
        
        $createCategory = $auth->createPermission('createCategory');
        $createCategory->description = 'Create a Category';
        $auth->add($createCategory);
        
        $updateCategory = $auth->createPermission('updateCategory');
        $updateCategory->description = 'Update Category';
        $auth->add($updateCategory);
        
        $deleteCategory = $auth->createPermission('deleteCategory');
        $deleteCategory->description = 'Delete Category';
        $auth->add($deleteCategory);

        // Permissions for Comment
        $manageComment = $auth->createPermission('manageComment');
        $manageComment->description = 'Manage Comment';
        $auth->add($manageComment);

        $viewComment = $auth->createPermission('viewComment');
        $viewComment->description = 'View Comment';
        $auth->add($viewComment);
        
        $createComment = $auth->createPermission('createComment');
        $createComment->description = 'Create a Comment';
        $auth->add($createComment);
        
        $updateComment = $auth->createPermission('updateComment');
        $updateComment->description = 'Update Comment';
        $auth->add($updateComment);
        
        $deleteComment = $auth->createPermission('deleteComment');
        $deleteComment->description = 'Delete Comment';
        $auth->add($deleteComment);

        // Permissions for Tag
        $manageTag = $auth->createPermission('manageTag');
        $manageTag->description = 'Manage Tag';
        $auth->add($manageTag);

        $viewTag = $auth->createPermission('viewTag');
        $viewTag->description = 'View Tag';
        $auth->add($viewTag);
        
        $createTag = $auth->createPermission('createTag');
        $createTag->description = 'Create a Tag';
        $auth->add($createTag);
        
        $updateTag = $auth->createPermission('updateTag');
        $updateTag->description = 'Update Tag';
        $auth->add($updateTag);
        
        $deleteTag = $auth->createPermission('deleteTag');
        $deleteTag->description = 'Delete Tag';
        $auth->add($deleteTag);

        // Add permission "updateOwnArticle" and attach rule to it.
        $updateOwnArticle = $auth->createPermission('updateOwnArticle');
        $updateOwnArticle->description = 'Update own Article';
        $updateOwnArticle->ruleName = $authorRule->name;
        $auth->add($updateOwnArticle);

        // Add permission "updateOwnProfile" and attach rule to it.
        $updateOwnProfile = $auth->createPermission('updateOwnProfile');
        $updateOwnProfile->description = 'Update Own Profile';
        $updateOwnProfile->ruleName = $userRule->name;
        $auth->add($updateOwnProfile);

        // Add permission "deleteOwnProfile" and attach rule to it.
        $deleteOwnProfile = $auth->createPermission('deleteOwnProfile');
        $deleteOwnProfile->description = 'Delete Own Profile';
        $deleteOwnProfile->ruleName = $userRule->name;
        $auth->add($deleteOwnProfile);

        // Add role "author" and give it permissions
        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $createArticle);
        $auth->addChild($updateOwnArticle, $updateArticle);
        $auth->addChild($author, $updateOwnArticle);
        $auth->addChild($updateOwnProfile, $updateUser);
        $auth->addChild($author, $updateOwnProfile);
        $auth->addChild($deleteOwnProfile, $deleteUser);
        $auth->addChild($author, $deleteOwnProfile);

        // добавляем роль "admin" и даём роли разрешение "updateArticle"
        // а также все разрешения роли "author"
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $manageArticle);
        $auth->addChild($admin, $viewArticle);
        $auth->addChild($admin, $updateArticle);
        $auth->addChild($admin, $deleteArticle);
        $auth->addChild($admin, $manageUser);
        $auth->addChild($admin, $viewUser);
        $auth->addChild($admin, $createUser);
        $auth->addChild($admin, $updateUser);
        $auth->addChild($admin, $deleteUser);
        $auth->addChild($admin, $manageCategory);
        $auth->addChild($admin, $viewCategory);
        $auth->addChild($admin, $createCategory);
        $auth->addChild($admin, $updateCategory);
        $auth->addChild($admin, $deleteCategory);
        $auth->addChild($admin, $manageComment);
        $auth->addChild($admin, $viewComment);
        $auth->addChild($admin, $createComment);
        $auth->addChild($admin, $updateComment);
        $auth->addChild($admin, $deleteComment);
        $auth->addChild($admin, $manageTag);
        $auth->addChild($admin, $viewTag);
        $auth->addChild($admin, $createTag);
        $auth->addChild($admin, $updateTag);
        $auth->addChild($admin, $deleteTag);
        $auth->addChild($admin, $author);

        // Назначение ролей пользователям. 1 - это ID,
        //  возвращаемый IdentityInterface::getId()
        // обычно реализуемый в модели User.
        $auth->assign($admin, 1);
    }
}