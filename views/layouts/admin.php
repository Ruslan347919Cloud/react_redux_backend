<?php
  use app\assets\AppAsset;
  use yii\helpers\Url;
  use app\models\Category;

  AppAsset::register($this);
  $categories = Category::getAll();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?=$this->title?></title>
        <?php $this->head() ?>
    </head>
    <body>
      <?php $this->beginBody() ?>
        <!--Header and navbar logo-->
        <header class="container-fluid mb-3 shadow">
            <nav class="navbar navbar-expand-md border navbar-light bg-primary row">
              <a class="navbar-brand" href="<?=Url::to(['site/index'])?>">My News Site</a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="navbar-toggler-icon"></i>
              </button>
              <div class="collapse navbar-collapse" id="navbarMenu">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                    <a class="nav-link text-white" href="<?=Url::to(['/site/index'])?>">Home</a>
                  </li>
                  <?php if (Yii::$app->user->can('admin')):?>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="<?=Url::to(['/admin/default/index'])?>">Admin</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="<?=Url::to(['/admin/article'])?>">Articles</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="<?=Url::to(['/admin/user'])?>">Users</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="<?=Url::to(['/admin/category'])?>">Categories</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="<?=Url::to(['/admin/tag'])?>">Tags</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="<?=Url::to(['/admin/comment'])?>">Comments</a>
                    </li>
                  <?php endif;?>
                  <?php if (!Yii::$app->user->isGuest):?>
                    <li class="nav-item">
                      <a href="<?=Url::to(['/admin/user/view', 'id' => Yii::$app->user->id])?>" class="nav-link text-white">
                            <?='My Profile ('.Yii::$app->user->identity->name.')'?>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="<?=Url::to(['/auth/logout'])?>" class="nav-link text-white">
                            Logout
                      </a>
                    </li>
                  <?php endif;?>
                </ul>
              </div>
            </nav>
        </header>
        <div class="wrapper">
            <div class="container-fluid content">
                <?=$content?>
            </div>
            <!--Footer-->
            <footer class="container-fluid">
                <div class="row justify-content-center bg-secondary text-white text-center">
                    <ul class="col-md-4 list-group list-group-flush">
                      <li class="list-group-item bg-secondary">Contact us:</li>
                      <li class="list-group-item bg-secondary">
                        <a href="mailto:#" class="text-white">mysite@test.com</a>
                      </li>
                      <li class="list-group-item bg-secondary">
                        <a href="tel:#" class="text-white">000-000-000</a>
                        </li>
                    </ul>
                    <ul class="col-md-4 list-group list-group-flush">
                      <li class="list-group-item bg-secondary">Friends:</li>
                      <li class="list-group-item bg-secondary">
                        <a href="#" class="text-white">www.somesite.com</a>
                      </li>
                      <li class="list-group-item bg-secondary">
                        <a href="#" class="text-white">www.anothersite.com</a>
                      </li>
                    </ul>
                </div>
            </footer>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>