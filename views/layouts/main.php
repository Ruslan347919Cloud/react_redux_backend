<?php
  use app\assets\AppAsset;
  use yii\helpers\Url;
  use app\models\Category;

  AppAsset::register($this);
  $categories = Category::getAll();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?=$this->title?></title>
        <?php $this->head() ?>
    </head>
    <body>
      <?php $this->beginBody() ?>
        <!--Header and navbar logo-->
        <header class="container-fluid mb-3 shadow">
            <nav class="navbar navbar-expand-sm border navbar-light bg-primary row">
              <a class="navbar-brand" href="<?=Url::to(['site/index'])?>">My News Site</a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="navbar-toggler-icon"></i>
              </button>
              <div class="collapse navbar-collapse" id="navbarMenu">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                    <a class="nav-link text-white" href="<?=Url::to(['site/index'])?>">Home</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Categories
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <?php foreach ($categories as $category):?>
                        <a class="dropdown-item" href="<?=Url::to(['site/category', 'id' => $category->id])?>">
                          <?=$category->title?>
                          <div class="dropdown-divider"></div>
                        </a>
                      <?php endforeach;?>
                    </div>
                  </li>
                  <?php if (Yii::$app->user->can('admin')):?>
                    <li class="nav-item">
                      <a href="<?=Url::to(['admin/default/index'])?>" class="nav-link text-white">Admin</a>
                    </li>
                  <?php endif;?>
                  <?php if (Yii::$app->user->isGuest):?>
                    <li class="nav-item">
                      <a href="<?=Url::to(['auth/login'])?>" class="nav-link text-white">Login</a>
                    </li>
                    <li class="nav-item">
                      <a href="<?=Url::to(['auth/signup'])?>" class="nav-link text-white">Register</a>
                    </li>
                  <?php else:?>
                    <li class="nav-item">
                      <a href="<?=Url::to(['/admin/user/view', 'id' => Yii::$app->user->id])?>" class="nav-link text-white">
                            <?='My Profile ('.Yii::$app->user->identity->name.')'?>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="<?=Url::to(['/auth/logout'])?>" class="nav-link text-white">
                            Logout
                      </a>
                    </li>
                  <?php endif;?>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="<?=Url::to(['site/about'])?>">About</a>
                  </li>
                </ul>
                <?=$this->render('/site/partials/site_search')?>
              </div>
            </nav>
        </header>
        <div class="wrapper">
            <div class="container-fluid content">
                <?=$content?>
            </div>
            <!--Footer-->
            <footer class="container-fluid">
                <div class="row justify-content-center bg-secondary text-white text-center">
                    <ul class="col-md-4 list-group list-group-flush">
                      <li class="list-group-item bg-secondary">Contact us:</li>
                      <li class="list-group-item bg-secondary">
                        <a href="mailto:#" class="text-white">mysite@test.com</a>
                      </li>
                      <li class="list-group-item bg-secondary">
                        <a href="tel:#" class="text-white">000-000-000</a>
                        </li>
                    </ul>
                    <ul class="col-md-4 list-group list-group-flush">
                      <li class="list-group-item bg-secondary">Friends:</li>
                      <li class="list-group-item bg-secondary">
                        <a href="#" class="text-white">www.somesite.com</a>
                      </li>
                      <li class="list-group-item bg-secondary">
                        <a href="#" class="text-white">www.anothersite.com</a>
                      </li>
                    </ul>
                </div>
            </footer>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>