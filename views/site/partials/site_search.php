<form action="/site/search" method="get" class="form-inline">
    <div class="input-group">
      <input name="searchQuery" class="form-control" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
          <button class="btn btn-outline-light" type="submit">
            <i class="fa fa-search" aria-hidden="true"></i>
          </button>
        </div>
    </div>
</form>