<?php
	use yii\helpers\Url;
  use yii\helpers\Html;
  use yii\helpers\StringHelper;
?>
<!--Latest-->
<div class="list-group mb-3 shadow sidebar__latest">
  <h5 class="list-group-item list-group-item-primary active">
      Latest
  </h5>
  <?php foreach ($latest as $item):?>
    <a href="<?=Url::to(['site/view-single', 'id' => $item['id']])?>" class="list-group-item list-group-item-action border-primary">
      <small><?=Html::encode($item['put_date'])?></small>
      <p class="mb-1">
        <?=StringHelper::truncate($item['content'], 100)?>
      </p>
    </a>
  <?php endforeach;?>
  <a href="#" class="list-group-item list-group-item-action text-center border-primary">
    More
  </a>
</div>
<!--Categories-->
<div class="list-group mb-3 shadow sidebar__categories">
  <h5 class="list-group-item list-group-item-primary active">
    Categories
  </h5>
  <?php foreach ($categories as $category):?>
    <a href="<?=Url::to(['site/category', 'id' => $category->id])?>" class="list-group-item list-group-item-action border-primary d-flex justify-content-between align-items-center">
      <span><?=Html::encode($category->title)?></span>
      <span class="badge badge-primary text-white">
        <?=$category->getArticlesCount()?>
      </span>
    </a>
  <?php endforeach;?>
</div>
<!--Authors-->
<div class="list-group mb-3 shadow sidebar__authors">
  <h5 class="list-group-item list-group-item-primary active">
      Authors
  </h5>
  <?php foreach ($authors as $author):?>
    <a href="<?=Url::to(['site/user-articles', 'id' => $author->id])?>" class="list-group-item list-group-item-action border-primary d-flex justify-content-between align-items-center p-2">
      <img src="<?=$author->getImage()?>" alt="" class="rounded">
      <span><?=Html::encode($author->name)?></span>
      <span class="badge badge-secondary">
        <?=$author->getArticlesCount()?>
      </span>
    </a>
  <?php endforeach;?>
  <a href="<?=Url::to(['admin/user/index'])?>" class="list-group-item list-group-item-action border-primary text-center">
    Show all
  </a>
</div>
<!--Tags-->
<div class="card border-primary shadow">
  <h5 class="card-header text-white bg-primary">
    Tags
  </h5>
  <div class="card-body">
    <?php foreach ($tags as $tag):?>
      <a href="<?=Url::to(['site/tag', 'id' => $tag->id])?>">
        <?=Html::encode($tag->title)?>
      </a>
    <?php endforeach;?>
  </div>
</div>