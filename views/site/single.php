<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use ymaker\social\share\widgets\SocialShare;

    $this->title = 'View Article';
?>
<div class="row">
	<!--Article-->
	<div class="col-md-7 col-lg-8 articles">
		<div class="card mb-3 shadow">
			<a href="#" class="p-1">
		  	<img src="<?=$article->getImage()?>" class="card-img-top" alt="">
			</a>
		  <div class="card-body">
		    <h5 class="card-title">
		    	<?=Html::encode($article->title)?>
	    	</h5>
		    <p class="card-text">
					<?=Html::encode($article->content)?>
		    </p>
		    <span class="d-flex flex-row-reverse">
					<?= SocialShare::widget([
				    'configurator'  => 'socialShare',
				    'url'           => Url::to(['site/view', 'id' => $article->id], true),
				    'title'         => $article->title,
				    'description'   => $article->description,
				    'imageUrl'      => Url::to($article->getImage(), true),
				    'containerOptions' => ['tag' => 'span',
				    	'class' => 'mr-2'],
				    'linkContainerOptions' => ['tag' => 'span',
				    	'class' => 'mr-2']
					]); ?>
				</span>
		  </div>
		  <div class="card-footer text-muted d-flex justify-content-between align-items-center">
		  	<small>
		  		<a href="#" class="card-link mr-2">
		  			By <?=Html::encode($article->user->name)?>
		  		</a>
		  		<span>
		  			<?=$article->getDate()?>
	  			</span>
		  	</small>
		    <span>
		    	<i class="fa fa-eye" aria-hidden="true"></i>
		    	<span><?=$article->viewed?></span>
		  	</span>
		  </div>
		</div>
		<!--Comments-->
		<ul class="list-unstyled mt-4">
			<?php if ($comments):?>
				<li>
					<h5 class="rounded-top bg-primary text-white shadow p-2">
						Comments
					</h5>
				</li>
				<?php foreach ($comments as $comment):?>
				  <li class="media bg-light rounded shadow my-2 p-1 comment">
				    <a href="<?=Url::to(['admin/user/view', 'id' => $comment->user->id])?>" class="comment__image align-self-center mr-3">
				    	<img src="<?=$comment->user->getImage()?>" class="img-fluid rounded" alt="">
				    </a>
				    <div class="media-body">
				      <h5 class="my-1">
				      	<a href="<?=Url::to(['admin/user/view', 'id' => $comment->user->id])?>">
				      		<?=Html::encode($comment->user->name)?>
			      		</a>
				      </h5>
				      <p>
				      	<?=Html::encode($comment->text)?>
				      </p>
				      <small><?=$comment->getDate()?></small>
				    </div>
				  </li>
				<?php endforeach;?>
			<?php else:?>
				<li>
					<h5 class="rounded-top bg-primary text-white shadow p-2">
						There is no comments for now.
					</h5>
				</li>
			<?php endif;?>
		</ul>
		<!--Comment form-->
	<?=$this->render('partials/comment_form', compact(
  	'article', 'commentModel'))?>
		<!--Comments pagination-->
    <?=LinkPager::widget([
      'pagination' => $pagination,
      'options' => [
          'tag' => 'ul',
          'class' => 'pagination justify-content-center'
      ],
      'pageCssClass' => 'page-item shadow',
      'nextPageCssClass' => 'page-item shadow',
      'prevPageCssClass' => 'page-item shadow',
      'activePageCssClass' => 'active',
      'disabledPageCssClass' => 'disabled',
      'linkOptions' => ['class' => 'page-link'],
      'disabledListItemSubTagOptions' => [
        'tag' => 'span',
        'class' => 'page-link'
      ]
    ])?>
	</div>
	<!--Sidebar-->
	<div class="col-md-5 col-lg-4 sidebar">
		<?=$this->render('partials/sidebar', compact(
			'latest', 'categories', 'tags', 'authors'))?>
	</div>
</div>
