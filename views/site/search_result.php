<?php
	use yii\helpers\Html;
	use yii\helpers\StringHelper;
	use yii\helpers\Url;

	$this->title = 'Search result';
?>
<div class="row">
	<!--Search results-->
	<div class="col-md-7 col-lg-8 articles">
		<?php if (empty($articles) && empty($users) && empty($comments)):?>
			<h4>No results.</h4>
		<?php else:?>
			<!--Articles-->
			<?php if ($articles):?>
				<div class="list-group shadow mb-3">
					<h4 class="list-group-item active">
						Articles:
					</h4>
					<?php foreach ($articles as $article):?>
						<a href ="<?=Url::to(['site/view-single', 'id' => $article->id])?>" class="list-group-item list-group-item-action">
							<h5>
								<?=Html::encode($article->title)?>
							</h5>
							<p>
								<?=StringHelper::truncate($article->content, 150)?>
							</p>
							<div class="d-flex justify-content-between">
								<small>
									By <?=Html::encode($article->user->name)?>.									
								</small>
								<small>
									<?=$article->getDate()?>
								</small>
							</div>
						</a>
					<?php endforeach;?>
				</div>
			<?php endif;?>
			<!--Users-->
			<?php if ($users):?>
				<div class="list-group shadow mb-3">
					<h4 class="list-group-item active">
						Users:
					</h4>
					<?php foreach ($users as $user):?>
						<a href ="#" class="list-group-item list-group-item-action">
							<?=Html::encode($user->name)?>
						</a>
					<?php endforeach;?>
				</div>
			<?php endif;?>
			<!--Comments-->
			<?php if ($comments):?>
				<div class="list-group shadow mb-3">
					<h4 class="list-group-item active">
						Comments:
					</h4>
					<?php foreach ($comments as $comment):?>
						<a href ="<?=Url::to(['site/view-single', 'id' => $comment->article_id])?>" class="list-group-item list-group-item-action">
							<div>
								<?=StringHelper::truncate($comment->text, 150)?>
							</div>
							<div class="d-flex justify-content-between">
								<small>
									By <?=Html::encode($comment->user->name)?>.									
								</small>
								<small>
									<?=Html::encode($comment->article->title)?>
								</small>
							</div>
						</a>
					<?php endforeach;?>
				</div>
			<?php endif;?>

		<?php endif;?>
	</div>
	<!--Sidebar-->
	<div class="col-md-5 col-lg-4 sidebar">
		<?=$this->render('partials/sidebar', compact(
      'latest', 'categories', 'tags', 'authors'))?>
	</div>
</div>