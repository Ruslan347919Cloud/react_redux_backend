<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\StringHelper;
    use yii\widgets\LinkPager;

    $this->title = 'Result';
?>
<div class="row">
	<!--Articles-->
	<div class="col-md-7 col-lg-8 articles">
		<?php if (!empty($articles)):?>
			<h4 class="bg-primary rounded-top shadow text-white p-2">
				Result:
			</h4>
			<?php foreach ($articles as $article):?>
				<div class="card border-primary shadow mb-3">
				  <a href="<?=Url::to(['site/view-single', 'id' => $article->id])?>" class="row no-gutters card-link text-dark">
				    <div class="col-md-4 align-self-center p-1">
				      <img src="<?=$article->getImage()?>" class="card-img" alt="">
				    </div>
				    <div class="col-md-8">
				      <div class="card-body">
				        <h5 class="card-title">
				        	<?=Html::encode($article->title)?>
			        	</h5>
				        <p class="card-text">
				        	<?=StringHelper::truncate($article->content, 200)?>
				      	</p>
				        <p class="card-text">
				        	<small class="text-muted">
				        		Last updated <?=$article->getLastUpdateTime()?>
				        	</small>
				        </p>
				      </div>
				    </div>
				  </a>
				</div>
			<?php endforeach;?>
		<?php else:?>
			<h4 class="bg-primary rounded-top shadow text-white p-2">
				No results.
			</h4>
		<?php endif;?>
		<!--Articles pagination-->
		<?=LinkPager::widget([
      'pagination' => $pagination,
      'options' => [
          'tag' => 'ul',
          'class' => 'pagination justify-content-center'
      ],
      'pageCssClass' => 'page-item shadow',
      'nextPageCssClass' => 'page-item shadow',
      'prevPageCssClass' => 'page-item shadow',
      'activePageCssClass' => 'active',
      'disabledPageCssClass' => 'disabled',
      'linkOptions' => ['class' => 'page-link'],
      'disabledListItemSubTagOptions' => [
        'tag' => 'span',
        'class' => 'page-link'
      ]
    ])?>
	</div>
	<!--Sidebar-->
	<div class="col-md-5 col-lg-4 sidebar">
		<?=$this->render('partials/sidebar', compact(
      'latest', 'categories', 'tags', 'authors'))?>
	</div>
</div>