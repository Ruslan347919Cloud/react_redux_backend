<?php
    use yii\helpers\Url;
    use yii\helpers\StringHelper;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;

    $this->title = 'My News Site';
?>
<div class="row">
  <?php if (Yii::$app->session->hasFlash('success')):?>
     <div class="alert alert-success alert-dismissible col-12" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <?php echo Yii::$app->session->getFlash('success'); ?>
     </div>
  <?php endif;?>
  <!--Articles-->
  <div class="col-md-7 col-lg-8 articles">
    <?php foreach ($articles as $article):?>
      <div class="card mb-3 shadow">
        <div class="card-header">
            Author: <?=Html::encode($article->user->name)?>
        </div>
        <a href="<?=Url::to(['site/view-single', 'id' => $article->id])?>" class="p-1">
          <img src="<?=$article->getImage()?>" class="card-img-top" alt="">
        </a>
        <div class="card-body">
          <h5 class="card-title">
            <?=Html::encode($article->title)?> 
          </h5>
          <p class="card-text">
            <?=StringHelper::truncate($article->content, 300)?>
          </p>
          <a href="<?=Url::to(['site/view-single', 'id' => $article->id])?>" class="card-link">Show all</a>
        </div>
        <div class="card-footer text-muted d-flex justify-content-between">
          <small>
            Last updated <?=$article->getLastUpdateTime()?>
          </small>
          <span>
            <i class="fa fa-eye" aria-hidden="true"></i>
            <span><?=$article->viewed?></span>
          </span>
        </div>
      </div>
    <?php endforeach;?>
    <!--Articles pagination-->
    <?=LinkPager::widget([
      'pagination' => $pagination,
      'options' => [
          'tag' => 'ul',
          'class' => 'pagination justify-content-center'
      ],
      'pageCssClass' => 'page-item shadow',
      'nextPageCssClass' => 'page-item shadow',
      'prevPageCssClass' => 'page-item shadow',
      'activePageCssClass' => 'active',
      'disabledPageCssClass' => 'disabled',
      'linkOptions' => ['class' => 'page-link'],
      'disabledListItemSubTagOptions' => [
        'tag' => 'span',
        'class' => 'page-link'
      ]
    ])?>
  </div>
  <!--Sidebar-->
  <div class="col-md-5 col-lg-4 sidebar">
    <?=$this->render('partials/sidebar', compact(
      'latest', 'categories', 'tags', 'authors'))?>
  </div>
</div>